import com.aspose.cells.Workbook;
import com.aspose.words.Document;
import com.aspose.words.FontSettings;

import java.io.File;
import java.io.FileOutputStream;

public class Office2PdfTest {
    public static void main(String[] args) {
        FontSettings.getDefaultInstance().setFontsFolder("/Users/liuzy/code/aspose-words-crack/fonts", false);
//        word("/Users/liuzy/Documents/附8-1：客服中心营销活动变更申请表-更新券名称.docx");
//        word("/Users/liuzy/Documents/05-中国银联全渠道支付平台-第2部分 产品接口规范-产品5 手机支付-手机控件支付.docx");
        xls("/Users/liuzy/Documents/公共基础软件使用情况统计表.xlsx");
        xls("/Users/liuzy/Documents/刘梦瑶_学生采集信息表.xls");
    }

    private static void word(String src) {
        System.out.println(src);
        File file = new File(src + ".pdf");
        try (FileOutputStream os = new FileOutputStream(file)) {
            long start = System.currentTimeMillis();
            Document doc = new Document(src);
            doc.save(os, com.aspose.words.SaveFormat.PDF);
            System.out.println("convert cost " + (System.currentTimeMillis() - start) + "ms");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void xls(String src) {
        System.out.println(src);
        File file = new File(src + ".pdf");
        try (FileOutputStream os = new FileOutputStream(file)) {
            long start = System.currentTimeMillis();
            Workbook doc = new Workbook(src);
            doc.save(os, com.aspose.cells.SaveFormat.PDF);
            System.out.println("convert cost " + (System.currentTimeMillis() - start) + "ms");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
